import config  from "../config/variables"
const baseUrl = `${config.api_url}`;

const getProducts = async() => {
    const response = await fetch(baseUrl+'/products');
    return response;
}; 

const productService = {
    getProducts,
};

export default productService;