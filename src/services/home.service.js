import config  from "../config/variables"
const baseUrl = `${config.api_url}`;

const getProductsHome = async() => {
    const response = await fetch(baseUrl+'/home');
    return response;
}; 

const homeService = {
    getProductsHome,
};

export default homeService;