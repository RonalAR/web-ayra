import React from "react";

const Footer = () => {
    return (
        <div className="container-fluid bg-white footer">
            <div className="container py-5">
                <div className="row g-5">
                    <div className="col-md-6 col-lg-8 wow fadeIn" data-wow-delay="0.1s">
                        <a href="index.html" className="d-inline-block mb-3">
                            <h1 className="text-secondary">AYRA</h1>
                        </a>
                        <p className="mb-0 fw-light">
                            Especialistas en Trajes a medida y Ropa de Alta Costura, Descubre
                            la excelencia en la confección de prendas que se adaptan a tu
                            estilo y personalidad. En nuestra sastrería, cada detalle es
                            cuidadosamente elaborado para ofrecerte la máxima calidad y
                            comodidad. ¡Haz una declaración de elegancia con nuestras
                            creaciones únicas!
                        </p>
                    </div>
                    <div className="col-md-6 col-lg-4 wow fadeIn" data-wow-delay="0.3s">
                        <h5 className="mb-4">Ponerse en contacto</h5>
                        <p>
                            <i className="fa fa-map-marker-alt me-3"></i>km 16 doble via la
                            guardia, Urbanizacion san ignacio
                        </p>
                        <p>
                            <i className="fa fa-phone-alt me-3"></i>+591 68788807
                        </p>
                        <p>
                            <i className="fa fa-envelope me-3"></i>L.ayra.r33@gmail.com
                        </p>
                    </div>
                </div>
                <div
                    className="container d-flex justify-content-center align-items-center pt-4"
                    data-wow-delay="0.1s"
                >
                    <div className="d-flex gap-3">
                        <a
                            className="btn btn-square btn-outline-secondary"
                            href="https://www.tiktok.com/@limber.ayra.revol"
                            target="_blank"
                        >
                            <i className="fab fa-tiktok"></i>
                        </a>
                        <a
                            className="btn btn-square btn-outline-secondary"
                            href="https://www.facebook.com/limber38"
                            target="_blank"
                        >
                            <i className="fab fa-facebook-f"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div className="container wow fadeIn" data-wow-delay="0.1s">
                <div className="copyright">
                    <div className="row">
                        <div className="text-center mb-3 mb-md-0">
                            &copy;{" "}
                            <a className="border-bottom" href="#">
                                Ayra
                            </a>
                            , All Right Reserved. Designed By{" "}
                            <a className="border-bottom" href="">
                                Ayra
                            </a>{" "}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;
