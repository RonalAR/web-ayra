import React from "react";
const Navbar = () => {
    return (
        <div className="navbar-wrapper">
            <div
                className="container-fluid sticky-top navbar-bg"
                style={{
                    background: "rgba(0,0,0,0.7)",
                    height: "80px",
                    display: "flex",
                    alignItems: "center",
                }}
            >
                <div className="container">
                    <nav
                        className="navbar navbar-expand-lg navbar-light"
                        style={{ height: "60px", padding: "0" }}
                    >
                        <a href="/">
                            <h2 className="text-white p-0 m-0">AYRA</h2>
                        </a>
                        <button
                            className="navbar-toggler ms-auto me-0 custom-button-burguer"
                            
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarCollapse"
                        >
                            <i className="fas fa-bars" style={{ color: "white" }}></i>
                        </button>
                        <div
                            className="collapse navbar-collapse style-navbar"
                            id="navbarCollapse"
                        >
                            <div className="navbar-nav ms-auto">
                                <NavItem href="/" text="Inicio" />
                                <NavItem href="/about" text="Sobre Nosotros" />
                                <NavItem href="/products" text="Productos" />
                                <NavItem href="/contact" text="Contacto" />
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    );
};

const NavItem = ({ href, text }) => {
    return (
        <a href={href} className="nav-item nav-link">
            {text}
        </a>
    );
};

export default Navbar;
