import React from "react";

const AboutContact = () => {
    return (
        <div className="container-fluid py-5">
            <div className="container">
                <div className="row g-4">
                    <div className="col-lg-4 wow fadeIn" data-wow-delay="0.1s">
                        <div className="feature-item position-relative bg-secondary text-center p-3">
                            <div className="border py-5 px-3" style={{ height: "220px" }}>
                                <i className="fa fa-map-marker-alt fa-3x text-dark mb-4"></i>
                                <h5 className="text-white">Ubicacion</h5>
                                <h5 className="fw-light text-white">
                                    km 16 doble via la guardia, Urbanizacion san ignacio
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 wow fadeIn" data-wow-delay="0.3s">
                        <div className="feature-item position-relative bg-secondary text-center p-3 h-251px">
                            <div className="border py-5 px-3" style={{ height: "220px" }}>
                                <i className="fa fa-phone-alt fa-3x text-dark mb-4"></i>
                                <h5 className="text-white">Llámanos</h5>
                                <h5 className="fw-light text-white">+591 68788807</h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 wow fadeIn" data-wow-delay="0.5s">
                        <div className="feature-item position-relative bg-secondary text-center p-3 h-251px">
                            <div className="border py-5 px-3" style={{ height: "220px" }}>
                                <i className="fa fa-envelope fa-3x text-dark mb-4"></i>
                                <h5 className="text-white">Envíenos un correo electrónico</h5>
                                <h5 className="fw-light text-white">L.ayra.r33@gmail.com</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutContact;
