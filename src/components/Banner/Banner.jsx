import React from "react";
import imgBanner from "../../assets/img/banner-image.png";

const Banner = () => {
    return (
        <div className="container-fluid bg-secondary hero-header mb-5">
            <div className="container">
                <div className="row g-5 align-items-center">
                    <div className="col-lg-6 text-center text-lg-start">
                        <h1 className="display-4 text-white animated slideInRight">
                            Especialistas en{" "}
                            <span className="fw-light text-dark">Trajes a medida</span> y{" "}
                            <span className="fw-light text-dark">Ropa de Alta Costura</span>
                        </h1>
                        <p className="text-white mb-4 animated slideInRight">
                            Descubre la excelencia en la confección de prendas que se adaptan
                            a tu estilo y personalidad. En nuestra sastrería, cada detalle es
                            cuidadosamente elaborado para ofrecerte la máxima calidad y
                            comodidad. ¡Haz una declaración de elegancia con nuestras
                            creaciones únicas!
                        </p>
                        <a
                            href="/products"
                            className="btn btn-dark py-2 px-4 me-3 animated slideInRight"
                        >
                            Explora Ahora
                        </a>

                        <a
                            href="/contact"
                            className="btn btn-outline-dark py-2 px-4 animated slideInRight"
                        >
                            Contactanos
                        </a>
                    </div>
                    <div className="col-lg-6 d-flex justify-content-center">
                        <img
                            className="img-fluid animated pulse infinite"
                            src={imgBanner}
                            alt=""
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Banner;
