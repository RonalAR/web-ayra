import React from "react";

const props = {
  name: "",
  price: "",
  img: "",
  in_offer: "",
  discount: "",
};
const calculate = (price, discount) => {
    const discountCalculate = (price * discount) / 100
    const priceTotal = price - discountCalculate
    return priceTotal
}

const CardProduct = ({ name, price, img, in_offer, discount } = props) => {
  return (
    <div className="product-item text-center p-4">
      <div className="mb-4">
        <img
          className="img-fluid"
          src={img}
          style={{ objectFit: "cover", height: "350px" }}
          alt=""
        />
      </div>
      <div className="mb-2">
        <small className="fa fa-star text-secondary"></small>
        <small className="fa fa-star text-secondary"></small>
        <small className="fa fa-star text-secondary"></small>
        <small className="fa fa-star text-secondary"></small>
        <small className="fa fa-star text-secondary"></small>
        <small>(5)</small>
      </div>
      <a href="" className="h6 d-inline-block mb-2">
        {name}
      </a>

      {in_offer === "yes" ? (
        <div>
          <h5
            style={in_offer === "yes" ? { color: "red" } : { color: "#6C757D" }}
          >
            {calculate (price, discount)} bs
          </h5>
          <h5 className="mb-3" style={{ color: "#6C757D" }}>
           <del>{price} bs</del>
          </h5>
        </div>
      ) : (
        <h5 className="mb-3" style={{ color: "#6C757D" }}>
          {in_offer ? price : price} bs
        </h5>
      )}

      <a href="contact" className="btn btn-outline-secondary px-3">
        Cotizar
      </a>
    </div>
  );
};

export default CardProduct;
