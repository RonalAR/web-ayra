import React from "react";
import img from "../../assets/img/about-start.png";

const AboutStart = () => {
  return (
    <div className="container-fluid py-5">
      <div className="container">
        <div className="row g-5 align-items-center">
          <div
            className="col-lg-6 wow fadeIn d-flex justify-content-center"
            data-wow-delay="0.1s"
          >
            <img className="img-fluid animated pulse infinite" src={img} />
          </div>
          <div className="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
            <h1 className="text-secondary mb-4">
              Descubren{" "}
              <span className="fw-light text-dark">
                Nuestra Amplia Colección de Trajes y Prendas de Vestir
              </span>
            </h1>
            <p className="mb-4">
              Explora Nuestra Variada Colección de Trajes y Prendas de Vestir
              que ofrecemos, diseñada para satisfacer todos los gustos y
              ocasiones. Desde elegantes trajes formales perfectos para eventos
              y reuniones importantes, hasta ropa casual y cómoda ideal para el
              día a día, nuestra selección abarca una variedad de estilos y
              tendencias. Además, contamos con una gama de tallas inclusivas,
              asegurando que cada cliente encuentre la prenda que mejor se
              ajuste a su cuerpo y personalidad. Navega por nuestras opciones y
              descubre la calidad y el diseño excepcional que nos distinguen..
            </p>
            <a className="btn btn-secondary py-2 px-4" href="/products">
              Ver Productos
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutStart;
