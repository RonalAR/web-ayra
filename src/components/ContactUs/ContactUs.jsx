
import React, { useState } from 'react';

const ContactUs = () => {
    const [formData, setFormData] = useState({
        name: "",
        email: "",
        subject: "",
        message: "",
    });


    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const { name, email, subject, message } = formData;
        const whatsappMessage = `Hola, soy ${name}. Mi correo es ${email}. El asunto es: ${subject}. Mensaje: ${message}`;
        const whatsappLink = `https://wa.me/+59168788807/?text=${encodeURIComponent(
            whatsappMessage
        )}`;
        window.location.href = whatsappLink;
    };
    return (
        <div class="container-fluid py-5">
            <div class="container">
                <div
                    class="mx-auto text-center wow fadeIn"
                    data-wow-delay="0.1s"
                    style={{ maxWidth: "600px" }}
                >
                    <h1 class="text-secondary mb-5">
                        <span class="fw-light text-dark">Si tiene alguna consulta, </span>{" "}
                        contáctenos
                    </h1>
                </div>
                <div class="row g-5">
                    <div class="col-lg-7 wow fadeIn" data-wow-delay="0.1s">
                        <div class="wow fadeIn" data-wow-delay="0.3s">
                            <form onSubmit={handleSubmit}>
                                <div className="row g-3">
                                    <div className="col-md-6">
                                        <div className="form-floating">
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="name"
                                                name="name"
                                                placeholder="Tu Nombre"
                                                value={formData.name}
                                                onChange={handleChange}
                                                required
                                            />
                                            <label htmlFor="name">Tu Nombre</label>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-floating">
                                            <input
                                                type="email"
                                                className="form-control"
                                                id="email"
                                                name="email"
                                                placeholder="Tu Email"
                                                value={formData.email}
                                                onChange={handleChange}
                                                required
                                            />
                                            <label htmlFor="email">Tu Email</label>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <div className="form-floating">
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="subject"
                                                name="subject"
                                                placeholder="Asunto"
                                                value={formData.subject}
                                                onChange={handleChange}
                                                required
                                            />
                                            <label htmlFor="subject">Asunto</label>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <div className="form-floating">
                                            <textarea
                                                className="form-control"
                                                placeholder="Deja un mensaje aquí"
                                                id="message"
                                                name="message"
                                                style={{ height: "150px" }}
                                                value={formData.message}
                                                onChange={handleChange}
                                                required
                                            ></textarea>
                                            <label htmlFor="message">Deja un mensaje aquí</label>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <button
                                            className="btn btn-secondary w-100 py-3"
                                            type="submit"
                                        >
                                            Enviar Mensaje
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-5 wow fadeIn" data-wow-delay="0.5s">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d4666.382188253814!2d-63.287902243985144!3d-17.91448098802548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zMTfCsDU0JzUyLjEiUyA2M8KwMTcnMDguNyJX!5e1!3m2!1ses-419!2sbo!4v1716008005732!5m2!1ses-419!2sbo"
                            frameborder="0"
                            style={{ minHeight: "380px", width: "100%", border: "0" }}
                            allowfullscreen=""
                            aria-hidden="false"
                            tabindex="0"
                        ></iframe>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContactUs;
