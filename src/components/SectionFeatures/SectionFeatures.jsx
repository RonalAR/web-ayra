import React from 'react';

const SectionFeatures = () => {
    return (
        <div className="container-fluid py-5">
            <div className="container">
                <div className="row g-4">
                    <div className="col-lg-4 wow fadeIn" data-wow-delay="0.1s">
                        <div className="feature-item position-relative bg-secondary text-center p-3">
                            <div className="border py-5 px-3" style={{height: "220px"}}>
                                <i className="fa fa-check-circle fa-3x text-dark mb-4"></i>
                                <h5 className="text-white mb-0">Tejidos de alta calidad</h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 wow fadeIn" data-wow-delay="0.3s">
                        <div className="feature-item position-relative bg-secondary text-center p-3 h-251px">
                            <div className="border py-5 px-3" style={{height: "220px"}}>
                                <i className="fa fa-user-tie fa-3x text-dark mb-4"></i>
                                <h5 className="text-white mb-0">Estilo clásico y versátil</h5>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 wow fadeIn" data-wow-delay="0.5s">
                        <div className="feature-item position-relative bg-secondary text-center p-3 h-251px">
                            <div className="border py-5 px-3" style={{height: "220px"}}>
                                <i className="fa fa-certificate  fa-3x text-dark mb-4"></i>
                                <h5 className="text-white mb-0">Experiencia y profesionalismo</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SectionFeatures;