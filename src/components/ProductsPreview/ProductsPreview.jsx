import React from "react";
import CardProduct from "../Cards/CartProduct";

const props = {
    products: [],
};

const ProductsPreview = ({ products } = props) => {
    return (
        <div className="container-fluid py-5">
            <div className="container">
                <div
                    className="mx-auto text-center wow fadeIn"
                    data-wow-delay="0.1s"
                    style={{ maxWidth: "600px" }}
                >
                    <h1 className="text-secondary mb-3">
                        <span className="fw-light text-dark">Prendas de vestir</span> de
                        alta calida
                    </h1>
                    <p className="mb-5">
                        Explore la colección y elija su prenda favorita. Una vez que haya
                        encontrado algo que le encante, Contactenos y con gusto le
                        proporcionaremos una cotización.
                    </p>
                </div>
                <div className="row g-4">
                    {products?.map((product, index) => (
                        <div className="col-md-6 col-lg-3 wow fadeIn" data-wow-delay="0.1s" key={index}>
                            <CardProduct
                                name={product.name}
                                price={product.price}
                                img={product.product_picture}
                                in_offer={product.in_offer}
                                discount={product.discount}
                            />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default ProductsPreview;
