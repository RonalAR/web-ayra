import React from 'react';
import SectionFeatures from '../../components/SectionFeatures/SectionFeatures';
import AboutStart from '../../components/AboutStart/AboutStart';

function About() {
  return (
    <div>
      <SectionFeatures/>
      <AboutStart/>
    </div>
  );
}

export default About;