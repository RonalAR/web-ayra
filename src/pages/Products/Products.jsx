import React, { useState, useEffect } from "react";
import ProductsPreview from "../../components/ProductsPreview/ProductsPreview";
import productService from "../../services/products.service";

function Productos() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await productService.getProducts();
      const data = await response.json();
      setProducts(data);
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  };

  return (
    <div>
      <ProductsPreview products={products} />
    </div>
  );
}

export default Productos;
