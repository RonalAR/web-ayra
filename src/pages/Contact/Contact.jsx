import React from 'react';
import AboutContact from "../../components/AboutContact/AboutContact";
import ContactUs from "../../components/ContactUs/ContactUs";

function Contact() {
  return (
    <div>
      <AboutContact/>
      <ContactUs/>
    </div>
  );
}

export default Contact;