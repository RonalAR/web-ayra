import React, { useState, useEffect } from "react";
import Banner from "../../components/Banner/Banner";
import SectionFeatures from "../../components/SectionFeatures/SectionFeatures";
import AboutStart from "../../components/AboutStart/AboutStart";
import ProductsPreview from "../../components/ProductsPreview/ProductsPreview";
import homeService from "../../services/home.service";

function Home() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await homeService.getProductsHome();
      const data = await response.json();
      setProducts(data);
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  };

  return (
    <div>
      <Banner />
      <SectionFeatures />
      <AboutStart />
      {products ? <ProductsPreview products={products} /> : <div></div>}
    </div>
  );
}

export default Home;
